import React, {Component} from 'react';
import {Line} from 'react-chartjs-2';
import {Table, Select, InputNumber, Spin, Icon} from 'antd';
import {IoArrowGraphUpRight, IoArrowGraphDownRight} from 'react-icons/lib/io';


class Content extends Component {
    render() {
        return (
            <div className="content">
                <Header/>
                <div className="main-container">
                    <div className="row ">
                        <Transactions doTransaction={this.props.doTransaction}/>
                        <Summary summaryData={this.props.summaryData}/>
                    </div>
                    <div className="row">
                        <History history={this.props.history}/>
                    </div>
                    <div className="row">
                        <Chart chartData={this.props.chartData}/>
                        <Chart chartData={this.props.portfolioChartData}/>
                    </div>
                    {/*<div className="row">*/}
                    {/*</div>*/}
                </div>
            </div>
        )
    }
}

class Header extends Component {
    render() {
        return (
            <div className="headerDiv">
                <h1>DASHBOARD</h1>
            </div>
        )
    }
}

class Transactions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buySymbol: 'BTC',
            buyQuantity: 0,

            sellSymbol: 'BTC',
            sellQuantity: 0
        }
    }

    resetState() {
        this.setState({
            buyQuantity: 0,
            sellQuantity: 0
        })
    };

    render() {
        const Option = Select.Option;
        return (
            <div className="container col-6">
                <div className="transactionCard">
                    <div className="container__header">
                        <h2>Transactions</h2>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="container__Content">
                                <div className="label">
                                    <label>Currency: </label>
                                </div>
                                <Select
                                    defaultValue={this.state.buySymbol}
                                    onChange={value => this.setState({buySymbol: value})}
                                >
                                    <Option value="BTC">BTC</Option>
                                    <Option value="ETH">ETH</Option>
                                    <Option value="XRP">XRP</Option>
                                </Select>

                                <div className="label">
                                    <label>Quantity: </label>
                                </div>
                                <InputNumber
                                    value={this.state.buyQuantity}
                                    onChange={value => this.setState({buyQuantity: value})}
                                    min={0}
                                />
                                <button
                                    className="btn btn-green"
                                    onClick={() => {
                                        this.props.doTransaction(this.state.buySymbol, this.state.buyQuantity);
                                        this.resetState();
                                    }}
                                >
                                    BUY
                                </button>
                            </div>
                        </div>
                        <div className="col-6">

                            <div className="container__Content">
                                <div className="label">
                                    <label>Currency: </label>
                                </div>
                                <Select
                                    defaultValue={this.state.sellSymbol}
                                    onChange={value => this.setState({sellSymbol: value})}
                                >
                                    <option value="BTC">BTC</option>
                                    <option value="ETH">ETH</option>
                                    <option value="XRP">XRP</option>
                                </Select>

                                <div className="label">
                                    <label>Quantity: </label>
                                </div>
                                <InputNumber
                                    value={this.state.sellQuantity}
                                    onChange={value => this.setState({sellQuantity: value})}
                                    min={0}
                                    width="100%"
                                />
                                <button
                                    className="btn"
                                    onClick={() => {
                                        this.props.doTransaction(this.state.sellSymbol,
                                            this.state.sellQuantity, true);
                                        this.resetState();
                                    }}
                                >
                                    SELL
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Summary extends Component {
    renderIcon = (data) => {
        if (!data)
            return <Icon type="loading" style={{ fontSize: '170%', color: 'blue' }}/>;
        return (
            data === "up"
                ? <IoArrowGraphUpRight color="green" size="30"/>
                : <IoArrowGraphDownRight color="red" size="30"/>
        )
    };

    render() {
        const columns = [{
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Last 24H',
            dataIndex: 'last',
            key: 'last',
        }];

        const dataSource = [{
            key: 'btc',
            name: 'BTC',
            last: this.renderIcon(this.props.summaryData["btc"])
        }, {
            key: 'eth',
            name: 'ETH',
            last: this.renderIcon(this.props.summaryData["eth"])
        }, {
            key: 'xrp',
            name: 'XRP',
            last: this.renderIcon(this.props.summaryData["xrp"])
        }];

        return (
            <div className="container col-6">
                <div className="summaryCard">
                    <div className="container__header">
                        <h2>Summary</h2>
                    </div>
                    <div className="container__Content">
                        <Table dataSource={dataSource} columns={columns} pagination={false}/>
                    </div>
                </div>
            </div>
        )
    }
}

class Chart extends Component {
    render() {
        const chartData = this.props.chartData;
        return (
            <div className="container col-12 chart">
                <div className="chartCard">
                    <div className="container__header">
                        <h2>Chart</h2>
                    </div>
                    <div className="container__Content">
                        <Spin spinning={chartData.labels.length === 0}>
                            <Line data={chartData} responsive="true"/>
                        </Spin>
                    </div>
                </div>
            </div>
        )
    }
}

class History extends Component {
    render() {
        const dataSource = this.props.history;

        const columns = [{
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
            filters: [{
                text: 'Purchase',
                value: 'Purchase',
            }, {
                text: 'Sell',
                value: 'Sell',
            }, {
                text: 'Reset',
                value: 'Reset',
            }],
            onFilter: (value, record) => record.type === value,
            render: (e) => <span className={e}>{e}</span>
        }, {
            title: 'Symbol',
            dataIndex: 'symbol',
            key: 'symbol',
            filters: [{
                text: 'BTC',
                value: 'BTC',
            }, {
                text: 'ETH',
                value: 'ETH',
            }, {
                text: 'XRP',
                value: 'XRP',
            }],
            onFilter: (value, record) => record.symbol === value,
        }, {
            title: 'Amount',
            dataIndex: 'amount',
            key: 'amount',
        }, {
            title: 'Timestamp',
            dataIndex: 'createdAt',
            key: 'createdAt',
            sorter: (a, b) => new Date(a.createdAt) - new Date(b.createdAt),
            render: (e) => <h2>{new Date(e).toLocaleString()}</h2>
        }];

        return (
            <div className="container col-12 history">
                <div className="chartCard">
                    <div className="container__header">
                        <h2>History</h2>
                    </div>
                    <div className="container__Content">
                        <Spin spinning={dataSource.length === 0}>
                            <Table rowKey={record => record.createdAt} dataSource={dataSource} columns={columns} scroll={{y: 290}}/>
                        </Spin>
                    </div>
                </div>
            </div>
        )
    }
}

// Here we export our component to be able to import in another -> App.js
export default Content;