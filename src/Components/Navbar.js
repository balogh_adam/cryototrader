import React, {Component} from 'react';

import {Select, Icon} from 'antd';

import logo from '../img/logo.png';
import userLogo from '../img/user-male-icon.png'


class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedBalance: "usd"
        }
    }

    render() {
        return (
            <div className="navbar">
                <div className="navbar__LogoDiv">
                    <img src={logo} alt="mainLogo"
                         className="navbar__Logo"/>
                </div>
                <ul className="navbar__Menu">
                    <li><a className="active" href="#">Dashboard</a></li>
                    <li><a className="" href="#">Settings</a></li>
                </ul>
                <div className="navbar__currentbalanceDiv">
                    <Select
                        defaultValue={this.state.selectedBalance}
                        onChange={value => this.setState({selectedBalance: value})}
                    >
                        <Select.Option value="usd">Dollar</Select.Option>
                        <Select.Option value="btc">BTC</Select.Option>
                        <Select.Option value="eth">ETH</Select.Option>
                        <Select.Option value="xrp">XRP</Select.Option>
                    </Select>
                    <h2>
                        {this.props.balance[this.state.selectedBalance].toFixed(2)}
                    </h2>
                </div>
                <div className="navbar__resetBtnDiv">
                    <button
                        className="btn resetbutton"
                        onClick={this.props.resetBalance}
                    >RESET
                    </button>
                </div>
                <div className="navbar__portfolio">
                    <h2>Portfolio value</h2>
                    {this.props.portfolio ?
                        (<h2>{(this.props.portfolio).toFixed(2)} $</h2>) :
                        (<Icon type="loading" style={{ fontSize: '150%' }}/>)}
                </div>
                <div className="navbar__loginDiv">
                    <img src={userLogo}
                         alt="userLogo"
                         className="userPic"/>
                    <h2>USER NAME</h2>

                    <button className="btn">Sign out</button>
                </div>
            </div>
        )
    }
}

export default Navbar;