import React, {Component} from 'react';
import {ToastContainer, toast} from 'react-toastify';
import 'antd/dist/antd.css';

import './App.css';
import Navbar from './Components/Navbar';
import Content from './Components/Content';


// Just random data for chart
const data = {
    labels: [],
    datasets: [
        {
            label: 'BTC',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#0ca94c',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
        },
        {
            label: 'ETH',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#d53e3c',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
        },
        {
            label: 'XRP',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#d4eb2b',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
        },
    ]
};
const portfolioData = {
    labels: [],
    datasets: [
        {
            label: 'PORTFOLIO',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#0ca94c',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
        },
    ]
};

// Our main component with the main state
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            balance: {
                usd: 0,
                btc: 0,
                eth: 0,
                xrp: 0
            },
            currentValues: {
                btc: 0,
                eth: 0,
                xrp: 0
            },
            portfolio: 0,
            summaryData: {
                btc: null,
                eth: null,
                xrp: null
            },
            history: [],
            chartData: data,
            portfolioChartData: portfolioData,
        };

    }

    componentDidMount() {
        const refreshMin = 5;
        this.refresh();
        this.getBalance();
        this.getHistory();
        this.interval = setInterval(() => this.refresh(), 60000 * refreshMin);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    refresh() {
        Promise.all([
            this.getExchangeRate('btc'),
            this.getExchangeRate('eth'),
            this.getExchangeRate('xrp')]).then((data) => {
            if (!data.includes(null))
                this.getPortfolio()
        });
    }

    // When we call this method the balance will be refreshed from the API
    getBalance = () => {
        // This way we can fetch the API so we can get the information what we need
        fetch('https://obudai-api.azurewebsites.net/api/account', {
                method: 'GET',
                headers: {
                    'X-Access-Token': '02837D0A-AB78-413D-9F0E-DA255117E3C4'
                }
            }

            // Change the response to json
        ).then(response => {
                if (response.status === 200)
                    return response.json();
                else {
                    toast.error("Ooops, something wrong happened! We can't get your balance now...");
                    return this.state.balance;
                }
            }

            // Change our state with the response data so the components can use these data
        ).then(data => {
            this.setState({
                balance: data,
            });
        })
    };

    getHistory = () => {
        fetch('https://obudai-api.azurewebsites.net/api/account/history', {
                method: 'GET',
                headers: {
                    'X-Access-Token': '02837D0A-AB78-413D-9F0E-DA255117E3C4'
                }
            }
        ).then(response => {
                if (response.status === 200)
                    return response.json();
                else {
                    return []
                }
            }
        ).then(data => {
            this.setState({
                history: data.reverse(),
            });
            if (data)
                this.refreshPortfolioChartData()
        })
    };

    getPortfolio = () => {
        let sum = this.state.balance.usd;
        Object.keys(this.state.currentValues).forEach((k) => {
            sum += this.state.balance[k] * this.state.currentValues[k];
        });

        this.setState({
            portfolio: sum
        })
    };

    resetBalance = () => {
        fetch('https://obudai-api.azurewebsites.net/api/account/reset', {
                method: 'POST',
                headers: {
                    'X-Access-Token': '02837D0A-AB78-413D-9F0E-DA255117E3C4',
                    'Content-Type': 'application/json'
                }
            }
        ).then(response => {
            if (response.status === 200) {
                this.getBalance();
                toast.success("Your balance has been reset!");
            }
            else {
                toast.error(`Ooops, something wrong happened! We couldn't reset your balance. Try again!`)
            }
        });
    };

    doTransaction = (symbol, amount, sell = false) => {
        let url = sell ?
            'https://obudai-api.azurewebsites.net/api/account/sell'
            : 'https://obudai-api.azurewebsites.net/api/account/purchase';

        fetch(url, {
                method: 'POST',
                headers: {
                    'X-Access-Token': '02837D0A-AB78-413D-9F0E-DA255117E3C4',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'Symbol': symbol,
                    'Amount': amount
                })
            }
        ).then(response => {
            if (response.status === 200) {
                this.getBalance();
                this.getPortfolio();
                this.getHistory();
                let action = sell ? 'sold' : 'purchased';
                toast.success(`You successfully ${action} ${amount} ${symbol}`);
            }
            else {
                toast.error(`Ooops, something wrong happened! Try again!`)
            }
        })

    };

    coinTrend = (data) => {
        const last = data[Object.keys(data)[0]];
        const previousDay = data[Object.keys(data)[288]];
        return last > previousDay ? 'up' : 'down';
    };

    getNumberFromSymbol = (symbol) => {
        if (symbol === 'btc') {
            return 0;
        }
        else if (symbol === 'eth') {
            return 1;
        }
        else {
            return 2;
        }
    };

    refreshChartData = (symbol, data) => {
        const newLabels = Object.keys(data).reverse().map((e) => new Date(e).toLocaleString());
        const newDataSet = Object.values(data).reverse();

        let newChartData = this.state.chartData;

        newChartData.labels = newLabels;
        newChartData.datasets[this.getNumberFromSymbol(symbol)].data = newDataSet;

        this.setState(() => ({
            chartData: newChartData
        }))
    };


    refreshPortfolioChartData = () => {
        let newChartData = this.state.portfolioChartData;
        const newLabels = Object.values(this.state.history).map((e) => new Date(e.createdAt).toLocaleString());
        const newDataSet = Object.values(this.state.history).map((e) => {
            console.log(e);
            if (e.balance && e.exchangeRates) {
                let sum = e.balance.usd;

                for (let coin in e.exchangeRates) {
                    sum += e.exchangeRates[coin] * e.balance[coin];
                }
                return sum;
            }
        });

        newChartData.datasets[0].data = newDataSet;
        newChartData.labels = newLabels;
        console.log(newChartData);
        this.setState(() => ({
            portfolioChartData: newChartData
        }))
    };

    getExchangeRate = (symbol) => {
        return fetch('https://obudai-api.azurewebsites.net/api/exchange/' + symbol, {
                method: 'GET',
                headers: {
                    'X-Access-Token': '02837D0A-AB78-413D-9F0E-DA255117E3C4'
                }
            }
        ).then(response => {
                if (response.status === 200)
                    return response.json();
                else
                    toast.error(`Ooops, something wrong happened! Can't get the exchange rate for ${symbol}!`);
                return null
            }
        ).then(data => {
            if (data && data.history && data.currentRate) {
                let newState = {};
                newState[symbol] = this.coinTrend(data.history);

                this.setState(prevState => ({
                    summaryData: {
                        ...prevState.summaryData,
                        [symbol]: this.coinTrend(data.history)
                    },
                    currentValues: {
                        ...prevState.currentValues,
                        [symbol]: data.currentRate
                    }
                }));
                return data;
            }
            else
                return null
        }).then(data => {
            if (data && data.history)
                this.refreshChartData(symbol, data.history);
            else
                return null
        })
    };

    render() {
        return (
            <div className="maincontent">
                {/* Here we can pass our main state to the inner component and there use it as props*/}
                {/* We can pass methods to change the main state from within */}
                <Navbar
                    balance={this.state.balance}
                    portfolio={this.state.portfolio}
                    resetBalance={this.resetBalance}
                />
                <Content
                    doTransaction={this.doTransaction}
                    chartData={this.state.chartData}
                    portfolioChartData={this.state.portfolioChartData}
                    summaryData={this.state.summaryData}
                    history={this.state.history}
                />
                <ToastContainer hideProgressBar={true}/>
            </div>
        );
    }
}

export default App;
